import React, { Component } from 'react';
import PropType from 'prop-types';
import classnames from 'classnames';
import ContentEditable from 'react-contenteditable';
import CellPlaceEnum from '../../enums/cellPlace';
import { IoIosAdd, IoIosClose } from 'react-icons/io';
import styles from './Cell.module.scss';

export default class extends Component {
  static propTypes = {
    cell: PropType.shape({
      id: PropType.string.isRequired,
      value: PropType.string.isRequired,
    }).isRequired,
    addSiblingCell: PropType.func.isRequired,
    updateCellValue: PropType.func.isRequired,
    removeCell: PropType.func.isRequired,
  };

  onValueChange = ({ target: { value } }) => {
    const {
      cell: { id },
      updateCellValue,
    } = this.props;

    updateCellValue(id, value);
  };

  render() {
    const {
      cell: { id, value },
      addSiblingCell,
      removeCell,
    } = this.props;

    return (
      <div className={styles.Cell}>
        <button
          className={classnames(
            `SmallButton`,
            styles.AddSiblingCellButton,
            styles.Before,
          )}
          title="Add cell before"
          onClick={() => {
            addSiblingCell(id, CellPlaceEnum.BEFORE, `before ${id}`);
          }}>
          <IoIosAdd />
        </button>

        <ContentEditable
          className={styles.Value}
          html={value}
          spellCheck="false"
          onChange={this.onValueChange}
        />

        <button
          className={classnames(
            `SmallButton`,
            styles.AddSiblingCellButton,
            styles.After,
          )}
          title="Add cell after"
          onClick={() => {
            addSiblingCell(id, CellPlaceEnum.AFTER, `after ${id}`);
          }}>
          <IoIosAdd />
        </button>

        <button
          className={classnames(`SmallButton`, styles.RemoveButton)}
          title="Remove cell"
          onClick={() => {
            removeCell(id);
          }}>
          <IoIosClose />
        </button>
      </div>
    );
  }
}
