import React from 'react';
import { connect } from 'react-redux';
import uniqid from 'uniqid';
import Table from '../components/Table/Table';
import { overwriteRowsWithDefaults } from '../actions/rows';

const TableContainer = props => <Table {...props} />;

const mapStateToProps = state => ({
  rows: state.rows,
});

const mapDispatchToProps = dispatch => ({
  overwriteRowsWithDefaults: () => {
    dispatch(overwriteRowsWithDefaults(uniqid('r'), uniqid('c')));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TableContainer);
