import {
  ADD_SIBLING_CELL,
  REMOVE_CELL,
  UPDATE_CELL_VALUE,
  OVERWRITE_ROWS_WITH_DEFAULTS,
  ADD_SIBLING_ROW,
  REMOVE_ROW,
  addSiblingCell,
  removeCell,
  updateCellValue,
  overwriteRowsWithDefaults,
  addSiblingRow,
  removeRow,
} from '../rows';

describe('rows actions', () => {
  it('should create ADD_SIBLING_CELL action', () => {
    const id = 'id';
    const relativeId = 'relativeId';
    const place = 'after';
    const value = 'some value';

    const expectedAction = {
      type: ADD_SIBLING_CELL,
      payload: {
        id,
        relativeId,
        place,
        value,
      },
    };

    expect(addSiblingCell(id, relativeId, place, value)).toEqual(
      expectedAction,
    );
  });

  it('should create REMOVE_CELL action', () => {
    const cellId = 'id';

    const expectedAction = {
      type: REMOVE_CELL,
      payload: {
        cellId,
      },
    };

    expect(removeCell(cellId)).toEqual(expectedAction);
  });

  it('should create UPDATE_CELL_VALUE action', () => {
    const cellId = 'id';
    const value = 'some value';

    const expectedAction = {
      type: UPDATE_CELL_VALUE,
      payload: {
        cellId,
        value,
      },
    };

    expect(updateCellValue(cellId, value)).toEqual(expectedAction);
  });

  it('should create OVERWRITE_ROWS_WITH_DEFAULTS action', () => {
    const rowId = 'rowId';
    const cellId = 'cellId';
    const value = 'some value';

    const expectedAction = {
      type: OVERWRITE_ROWS_WITH_DEFAULTS,
      payload: {
        rowId,
        cellId,
        value,
      },
    };

    expect(overwriteRowsWithDefaults(rowId, cellId, value)).toEqual(
      expectedAction,
    );
  });

  it('should create ADD_SIBLING_ROW action', () => {
    const id = 'id';
    const relativeId = 'relativeId';
    const cellId = 'cellId';
    const place = 'after';
    const cellValue = 'some value';

    const expectedAction = {
      type: ADD_SIBLING_ROW,
      payload: {
        id,
        relativeId,
        cellId,
        place,
        cellValue,
      },
    };

    expect(addSiblingRow(id, relativeId, cellId, place, cellValue)).toEqual(
      expectedAction,
    );
  });

  it('should create REMOVE_ROW action', () => {
    const rowId = 'rowId';

    const expectedAction = {
      type: REMOVE_ROW,
      payload: {
        rowId,
      },
    };

    expect(removeRow(rowId)).toEqual(expectedAction);
  });
});
