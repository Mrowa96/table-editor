import React from 'react';
import { connect } from 'react-redux';
import uniqid from 'uniqid';
import { addSiblingRow, removeRow } from '../actions/rows';
import Row from '../components/Row/Row';

const RowContainer = props => <Row {...props} />;

const mapDispatchToProps = dispatch => ({
  addSiblingRow: (relativeId, place) => {
    dispatch(addSiblingRow(uniqid('r'), relativeId, uniqid('c'), place));
  },
  removeRow: rowId => {
    dispatch(removeRow(rowId));
  },
});

export default connect(
  null,
  mapDispatchToProps,
)(RowContainer);
