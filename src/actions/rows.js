import CellPlaceEnum from '../enums/cellPlace';
import RowPlaceEnum from '../enums/rowPlace';

export const ADD_SIBLING_CELL = 'ADD_SIBLING_CELL';
export const REMOVE_CELL = 'REMOVE_CELL';
export const UPDATE_CELL_VALUE = 'UPDATE_CELL_VALUE';
export const OVERWRITE_ROWS_WITH_DEFAULTS = 'OVERWRITE_ROWS_WITH_DEFAULTS';
export const ADD_SIBLING_ROW = 'ADD_SIBLING_ROW';
export const REMOVE_ROW = 'REMOVE_ROW';

export function addSiblingCell(
  id,
  relativeId,
  place = CellPlaceEnum.AFTER,
  value = 'New cell',
) {
  return {
    type: ADD_SIBLING_CELL,
    payload: {
      id,
      relativeId,
      place,
      value,
    },
  };
}

export function removeCell(cellId) {
  return {
    type: REMOVE_CELL,
    payload: {
      cellId,
    },
  };
}

export function updateCellValue(cellId, value) {
  return {
    type: UPDATE_CELL_VALUE,
    payload: {
      cellId,
      value,
    },
  };
}

export function overwriteRowsWithDefaults(rowId, cellId, value = 'New cell') {
  return {
    type: OVERWRITE_ROWS_WITH_DEFAULTS,
    payload: {
      rowId,
      cellId,
      value,
    },
  };
}

export function addSiblingRow(
  id,
  relativeId,
  cellId,
  place = RowPlaceEnum.AFTER,
  cellValue = 'New cell',
) {
  return {
    type: ADD_SIBLING_ROW,
    payload: {
      id,
      relativeId,
      cellId,
      place,
      cellValue,
    },
  };
}

export function removeRow(rowId) {
  return {
    type: REMOVE_ROW,
    payload: {
      rowId,
    },
  };
}
