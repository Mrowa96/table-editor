const RowPlaceEnum = {
  BEFORE: 'before',
  AFTER: 'after',
};

export default RowPlaceEnum;
