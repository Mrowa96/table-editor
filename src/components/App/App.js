import React from 'react';
import TableContainer from '../../containers/TableContainer';
import styles from './App.module.scss';

export default () => (
  <div className={styles.App}>
    <TableContainer />
  </div>
);
