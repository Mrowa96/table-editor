import React from 'react';
import PropType from 'prop-types';
import RowContainer from '../../containers/RowContainer';
import ExportCsvButton from '../ExportCsvButton/ExportCsvButton';
import styles from './Table.module.scss';

const Table = ({ rows, overwriteRowsWithDefaults }) => {
  if (!Array.isArray(rows)) {
    return null;
  }

  const hasRows = !!rows.length;

  return (
    <div className={styles.Table}>
      <div className={styles.Content}>
        {hasRows &&
          rows.map((row, index) => (
            <RowContainer key={row.id} row={row} isFirst={index === 0} />
          ))}
        {!hasRows && (
          <>
            <span>Rows not found.</span>
            <button
              className={styles.AddRowButton}
              onClick={overwriteRowsWithDefaults}>
              Add first row
            </button>
          </>
        )}
      </div>
      {hasRows && (
        <div className={styles.Actions}>
          <ExportCsvButton rows={rows} />
        </div>
      )}
    </div>
  );
};

Table.propTypes = {
  rows: PropType.arrayOf(
    PropType.shape({
      id: PropType.string.isRequired,
      cells: PropType.arrayOf(
        PropType.shape({
          id: PropType.string.isRequired,
          value: PropType.string.isRequired,
        }),
      ),
    }),
  ),
  overwriteRowsWithDefaults: PropType.func.isRequired,
};

export default Table;
