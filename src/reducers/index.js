import { combineReducers } from 'redux';
import { rows } from './rows';

const reducers = combineReducers({
  rows,
});

export default reducers;
