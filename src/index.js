import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import App from './components/App/App';
import reducers from './reducers/index';
import { defaultStore } from './store/defaultStore';
import * as serviceWorker from './serviceWorker';
import './index.css';

const store = createStore(reducers, defaultStore);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

if (process.env.NODE_ENV === 'development') {
  store.subscribe(() => {
    console.log(store.getState());
  });
}

if (process.env.NODE_ENV === 'production') {
  serviceWorker.register();
}
