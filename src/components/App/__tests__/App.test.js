import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';
import Table from '../../Table/Table';

it('should render Table component', () => {
  const component = shallow(<App />);

  expect(component.find(Table)).toBeTruthy();
});
