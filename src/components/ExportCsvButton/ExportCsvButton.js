import React, { PureComponent } from 'react';
import PropType from 'prop-types';
import { IoIosDownload } from 'react-icons/io';
import triggerFileDownload from '../../utils/triggerFileDownload';
import convertObjectToCsv from '../../utils/convertObjectToCsv';
import styles from './ExportCsvButton.module.scss';

export default class ExportCsvButton extends PureComponent {
  static propTypes = {
    rows: PropType.arrayOf(
      PropType.shape({
        id: PropType.string.isRequired,
        cells: PropType.arrayOf(
          PropType.shape({
            id: PropType.string.isRequired,
            value: PropType.string.isRequired,
          }),
        ),
      }),
    ),
  };

  onButtonClick = () => {
    const { rows } = this.props;
    const data = convertObjectToCsv(rows);
    const fileName = `${new Date().toLocaleDateString()}_${new Date().toLocaleTimeString()}.csv`;
    const type = 'data:text/csv';

    triggerFileDownload(data, fileName, type);
  };

  render() {
    return (
      <button
        onClick={this.onButtonClick}
        className={styles.Button}
        title="Export to CSV">
        <IoIosDownload />
      </button>
    );
  }
}
