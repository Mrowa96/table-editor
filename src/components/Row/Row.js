import React, { Component } from 'react';
import PropType from 'prop-types';
import classnames from 'classnames';
import CellContainer from '../../containers/CellContainer';
import RowPlaceEnum from '../../enums/rowPlace';
import { IoIosAdd, IoIosClose } from 'react-icons/io';
import styles from './Row.module.scss';

export default class Row extends Component {
  static propTypes = {
    row: PropType.shape({
      id: PropType.string.isRequired,
      cells: PropType.arrayOf(
        PropType.shape({
          id: PropType.string.isRequired,
          value: PropType.string.isRequired,
        }),
      ),
    }).isRequired,
    isFirst: PropType.bool.isRequired,
    addSiblingRow: PropType.func.isRequired,
  };

  render() {
    const { row, isFirst, addSiblingRow, removeRow } = this.props;

    if (!Array.isArray(row.cells) || !row.cells.length) {
      return null;
    }

    return (
      <div className={classnames(styles.Row, { [styles.isFirst]: !!isFirst })}>
        <div className={styles.ButtonWrapper}>
          <button
            className={classnames(`SmallButton`, styles.AddRowButton)}
            title="Add row before"
            onClick={() => {
              addSiblingRow(row.id, RowPlaceEnum.BEFORE, `before ${row.id}`);
            }}>
            <IoIosAdd />
          </button>
        </div>

        <div className={styles.CellsWrapper}>
          {row.cells.map(cell => (
            <CellContainer key={cell.id} cell={cell} />
          ))}
        </div>

        <button
          className={classnames(`SmallButton`, styles.RemoveButton)}
          title="Remove row"
          onClick={() => {
            removeRow(row.id);
          }}>
          <IoIosClose />
        </button>

        <div className={styles.ButtonWrapper}>
          <button
            className={classnames(`SmallButton`, styles.AddRowButton)}
            title="Add row after"
            onClick={() => {
              addSiblingRow(row.id, RowPlaceEnum.AFTER, `after ${row.id}`);
            }}>
            <IoIosAdd />
          </button>
        </div>
      </div>
    );
  }
}
