import { defaultStore } from '../store/defaultStore';
import {
  ADD_SIBLING_CELL,
  REMOVE_CELL,
  UPDATE_CELL_VALUE,
  OVERWRITE_ROWS_WITH_DEFAULTS,
  ADD_SIBLING_ROW,
  REMOVE_ROW,
} from '../actions/rows';
import CellPlaceEnum from '../enums/cellPlace';
import RowPlaceEnum from '../enums/rowPlace';

export function rows(state = defaultStore.rows, action) {
  let parentRow;

  switch (action.type) {
    case ADD_SIBLING_CELL:
      return state.map(row => {
        return {
          ...row,
          cells: row.cells.reduce((output, cell) => {
            if (cell.id === action.payload.relativeId) {
              if (action.payload.place === CellPlaceEnum.BEFORE) {
                output.push({
                  id: action.payload.id,
                  value: action.payload.value,
                });
                output.push({ ...cell });
              } else {
                output.push({ ...cell });
                output.push({
                  id: action.payload.id,
                  value: action.payload.value,
                });
              }
            } else {
              output.push({ ...cell });
            }

            return output;
          }, []),
        };
      });

    case REMOVE_CELL:
      parentRow = state.find(row =>
        row.cells.find(cell => cell.id === action.payload.cellId),
      );

      if (parentRow) {
        if (parentRow.cells.length === 1) {
          return state.filter(row => row !== parentRow);
        } else {
          return state.map(row => {
            if (row === parentRow) {
              return {
                ...row,
                cells: row.cells.filter(
                  cell => cell.id !== action.payload.cellId,
                ),
              };
            }

            return { ...row };
          });
        }
      }

      return state;

    case UPDATE_CELL_VALUE:
      return state.map(row => {
        return {
          ...row,
          cells: row.cells.reduce((output, cell) => {
            if (cell.id === action.payload.cellId) {
              output.push({ ...cell, value: action.payload.value });
            } else {
              output.push({ ...cell });
            }

            return output;
          }, []),
        };
      });

    case OVERWRITE_ROWS_WITH_DEFAULTS:
      return [
        {
          id: action.payload.rowId,
          cells: [{ id: action.payload.cellId, value: action.payload.value }],
        },
      ];

    case ADD_SIBLING_ROW:
      return state.reduce((output, row) => {
        if (row.id === action.payload.relativeId) {
          if (action.payload.place === RowPlaceEnum.BEFORE) {
            output.push({
              id: action.payload.id,
              cells: [
                { id: action.payload.cellId, value: action.payload.cellValue },
              ],
            });
            output.push({ ...row });
          } else {
            output.push({ ...row });
            output.push({
              id: action.payload.id,
              cells: [
                { id: action.payload.cellId, value: action.payload.cellValue },
              ],
            });
          }
        } else {
          output.push({ ...row });
        }

        return output;
      }, []);

    case REMOVE_ROW:
      return state.filter(row => row.id !== action.payload.rowId);

    default:
      return state;
  }
}
