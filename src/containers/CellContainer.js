import React from 'react';
import { connect } from 'react-redux';
import uniqid from 'uniqid';
import { addSiblingCell, removeCell, updateCellValue } from '../actions/rows';
import Cell from '../components/Cell/Cell';

const CellContainer = props => <Cell {...props} />;

const mapDispatchToProps = dispatch => ({
  addSiblingCell: (relativeId, value) => {
    dispatch(addSiblingCell(uniqid('c'), relativeId, value));
  },
  removeCell: cellId => {
    dispatch(removeCell(cellId));
  },
  updateCellValue: (cellId, value) => {
    dispatch(updateCellValue(cellId, value));
  },
});

export default connect(
  null,
  mapDispatchToProps,
)(CellContainer);
