const convertObjectToCsv = rows => {
  return rows.reduce((output, { cells }) => {
    let stringifiedRow = Object.values(cells).reduce(
      (output, { value }, index) => {
        let innerValue = value === null ? '' : value.toString();

        if (value instanceof Date) {
          innerValue = value.toLocaleString();
        }

        let result = innerValue.replace(/"/g, '""');

        if (result.search(/("|,|\n)/g) >= 0) {
          result = '"' + result + '"';
        }

        if (index > 0) {
          output += ',';
        }

        output += result;

        return output;
      },
      '',
    );

    output += stringifiedRow + '\n';

    return output;
  }, '');
};

export default convertObjectToCsv;
