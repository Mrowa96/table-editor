import React from 'react';
import { shallow } from 'enzyme';
import Table from '../Table';
import RowContainer from '../../../containers/RowContainer';

describe('<Table/>', () => {
  let component,
    overwriteRowsWithDefaults = jest.fn();

  beforeAll(() => {
    component = shallow(
      <Table overwriteRowsWithDefaults={overwriteRowsWithDefaults} />,
    );
  });

  it('should render null if rows are undefined', () => {
    expect(component.html()).toEqual(null);
  });

  it('should render message that is no rows', () => {
    component.setProps({ rows: [] });

    expect(component.html().includes('Rows not found')).toBeTruthy();
  });

  it('should render exactly 2 rows', () => {
    component.setProps({
      rows: [{ id: 'r1', cells: [] }, { id: 'r2', cells: [] }],
    });

    expect(component.find(RowContainer).length).toEqual(2);
  });
});
