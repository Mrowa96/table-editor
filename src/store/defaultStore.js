import uniqid from 'uniqid';

export const defaultStore = {
  rows: [
    {
      id: uniqid('r'),
      cells: [
        { id: uniqid('c'), value: 'Very short text' },
        { id: uniqid('c'), value: 'A little bit longer text about nothing' },
        {
          id: uniqid('c'),
          value: `Lorem ipsum dolor sit amet, 
          consectetur adipiscing elit, 
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, 
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        },
      ],
    },
    {
      id: uniqid('r'),
      cells: [
        {
          id: uniqid('c'),
          value: `Lorem ipsum dolor sit amet, 
          consectetur adipiscing elit, 
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, 
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,
        },
      ],
    },
  ],
};
