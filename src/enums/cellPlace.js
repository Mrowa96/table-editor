const CellPlaceEnum = {
  BEFORE: 'before',
  AFTER: 'after',
};

export default CellPlaceEnum;
